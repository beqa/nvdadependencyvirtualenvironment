Welcome to a hopefully slightly easier way of bootstrapping your NVDA development environment.
To get this set up:

Install Python 2.7 32-bit from python.org

Unzip the folder on top of the root of your NVDA source distribution.

Run the Boost installer:
  On the components page of the installer, make sure to install at least all of the defaults (whatever is already checked).

Run the Windows SDK Installer
  You need to install both the 32 bit and 64 bit libraries and tools.

Launch the command line and cd into the root of the NVDA source distribution.

Run env\scripts\activate

Then proceed as you regularly would, I.E. by running scons source


This package contains:
Comtypes 0.6.2
wxPython 2.8.12.1 Unicode
Python Windows Extensions build 218
eSpeak 1.47.11 
IAccessible2 IDL 1.2.1
ConfigObj 4.7.0
liblouis 2.5.2
NVDA Media 2012-05-04-01
Required system dlls
Adobe Acrobat accessibility interface
Adobe FlashAccessibility interface typelib
txt2tags version 2.6
The Microsoft Windows 7 SDK installer (this must be installed manually!)
MinHook 1.1.0
The Boost C++ libraries version 1.47 installer (Must be installed manually)
SCons 2.3.0
brlapi 0.5.5 (Note: This may not work because of a virtualenv issue, contact me if it does not)
ALVA BC6 generic dll
lilli.dll 2.1.0.0
PySerial, enabling the Baum, Brailliant B, hedo, Papenmeier and/or Seika braille display drivers:
HanSoneConnect.dll 2.0.0.1
SyncBraille.dll 1.0.0.1
Python interface to FTDI driver/chip for the Papenmeier braille display driver
epydoc 3.0.1+patch for bug 2585292
Doxygen 1.8.3.1
xgettext and msgfmt from GNU gettext:
